import controlP5.*; //<>//
import java.awt.Frame;
import java.awt.BorderLayout;
import java.util.List;
import processing.serial.*;

ControlP5 cp5;
DropdownList portSelect;
Textfield baudSelect;
Button connectButton;
ColorWheel colorPicker;
RadioButton ledSelector;
Textlabel errorLabel;

Serial serial;

void setup() {
    surface.setTitle("LED Remote Control");
    size(300, 270);
    stroke(0);
    cp5 = new ControlP5(this);

    baudSelect = cp5.addTextfield("baudSelect")
        .setPosition(160, 10)
        .setSize(60, 20)
        .setLabel("Baud Rate")
        .setAutoClear(false)
        .setInputFilter(ControlP5.INTEGER)
        .setValue("115200");

    connectButton = cp5.addButton("connectButton")
        .setPosition(230, 10)
        .setSize(60, 20)
        .setLabel("Connect");
    
    colorPicker = cp5.addColorWheel("colorPicker")
    	.setPosition(10, 50)
    	.setRGB(color(72, 190, 239))
    	.setAlpha(255)
    	.setLabel("Color")
    	.hide();
    
    errorLabel = cp5.addTextlabel("errorLabel")
        .setPosition(10, 100)
        .setColorValue(color(255, 0, 0))
        .setWidth(280)
        .setHeight(100)
        .setMultiline(true);
    
    ledSelector = cp5.addRadioButton("ledSelector")
    	.setPosition(240, 50)
    	.setItemHeight(20)
    	.setLabel("LEDs")
    	.addItem("All", 0)
    	.addItem("Even", 1)
    	.addItem("Odd", 2)
    	.activate(0) //<>//
    	.hide();
    
    portSelect = cp5.addDropdownList("portSelect")
        .setPosition(10, 10)
        .setSize(140, 200)
        .setBarHeight(20)
        .setLabel("Serial Port")
        .close();
    
	listSerialPorts();
}

void draw() {
    background(0, 0, 0);
    
    if (serial != null && serial.available() > 0) {
        System.out.print("Device: " + serial.readString());
    }
}

public void connectButton() {
    if (serial == null) {
        connect();
    } else {
        disconnect();
    }
}

public void controlEvent(ControlEvent event) {
    if (event.isFrom(colorPicker)) {
        String hex = hex(colorPicker.getRGB());
        hex = hex.substring(2);
        
        String command = getLedSelection().toString() + "#" + hex;
        System.out.println("Control: " + command);
        
        try {
        	serial.write(command + "\r\n");
        } catch (Exception ex) {
            System.err.println(ex.toString());
        }
    }
}

void listSerialPorts() {
    portSelect.clear();

    for (String port : Serial.list()) {
        portSelect.addItem(port, port);
    }
}

Integer getLedSelection() {
    int index = 0;
    
    for (int i = 0; i < ledSelector.getItems().size(); i++) {
        if (ledSelector.getState(i)) {
            index = i;
        }
    }
    
    return -(index + 1);
}

void connect() {
    String port = portSelect.getItem((int) portSelect.getValue()).get("text").toString();
    int baud = Integer.parseInt(baudSelect.getText());
    
    try {
    	serial = new Serial(this, port, baud);
    	assert(serial.active());
    	errorLabel.setText("");
    } catch (Exception ex) {
      	errorLabel.setText(ex.toString());
        disconnect();
        return;
    }
    
    connectButton.setLabel("Disconnect");
	colorPicker.show();
    ledSelector.show();
}


void disconnect() {
    serial.stop();
    serial = null;
    connectButton.setLabel("Connect");
    colorPicker.hide();
    ledSelector.hide();
    listSerialPorts();
}
