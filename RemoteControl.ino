#include <FastLED.h>

const byte PROGMEM PIN_LED_DATA = 2;
const byte PROGMEM LED_COUNT = 10;

CRGB leds[LED_COUNT];

void setup() {
	Serial.begin(115200);
	FastLED.addLeds<WS2812, PIN_LED_DATA, GRB>(leds, LED_COUNT);
	FastLED.showColor(CRGB::Black);
}

CRGB parse_hex(String hex) {
	const char* string = hex.c_str();
	long num = strtol(&string[1], NULL, 16);
	
	byte r = num >> 16;
	byte g = num >> 8 & 0xFF;
	byte b = num & 0xFF;
	
	return CRGB(r, g, b);
}

void loop() {
	while (Serial.available()) {
		String input = Serial.readStringUntil('\n');
		int boundary = input.indexOf('#');
		
		int led = input.substring(0, boundary).toInt();
		String hex = input.substring(boundary);
		CRGB color = parse_hex(hex);

		switch (led) {
			case -3:
				Serial.print(F("Odd"));
				
				for (int i = 1; i < LED_COUNT; i += 2) {
					leds[i] = color;
				}
				
				break;
				
			case -2:
				Serial.print(F("Even"));
				
				for (int i = 0; i < LED_COUNT; i += 2) {
					leds[i] = color;
				}
				
				break;
				
			case -1:
				Serial.print(F("All"));
				
				for (int i = 0; i < LED_COUNT; i++) {
					leds[i] = color;
				}
				
				break;
				
			default:
				Serial.print(F("LED "));
				Serial.print(led);
				
				leds[led] = color;
				
				break;
		}

		FastLED.show();
		Serial.print(F(" = "));
		Serial.println(hex);
	}
}

